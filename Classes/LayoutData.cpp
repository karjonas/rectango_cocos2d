#include "LayoutData.h"
#include <math.h>

int32_t LayoutData::calculate_scaling(cocos2d::Size screen_size)
{
  int scaling_x = static_cast<int>(screen_size.width) / max_x;
  int scaling_y = static_cast<int>(screen_size.height) / max_y;
  return std::max(1, std::min(scaling_x, scaling_y));
}
