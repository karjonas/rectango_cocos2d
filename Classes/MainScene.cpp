#include "MainScene.h"
#include "AppMacros.h"
#include "LevelCollection.h"
#include "Unit.h"
#include "Board.h"
#include "LayoutData.h"

USING_NS_CC;


Scene* MainScene::scene()
{
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  MainScene *layer = MainScene::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool MainScene::init()
{
  cocos2d::Color4B background_color(77, 83, 60, 255);

  //////////////////////////////
  // 1. super init first
  if ( !LayerColor::initWithColor(background_color))
  {
    return false;
  }

  //Initializing and binding
  auto listener = EventListenerKeyboard::create();

  listener->onKeyPressed = [this](EventKeyboard::KeyCode kc, Event* event){keyPressed(kc,event);};
  listener->onKeyReleased = [this](EventKeyboard::KeyCode kc, Event* event){keyReleased(kc,event);};

  _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

  scheduleUpdate();

  auto director = Director::getInstance();
  auto glview = director->getOpenGLView();

  this->setScale(LayoutData::calculate_scaling(glview->getFrameSize()));

  main_loop_actor.create((*this));

  return true;
}

void MainScene::menuCloseCallback(Ref* sender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void MainScene::keyPressed(EventKeyboard::KeyCode keyCode, Event *event)
{
  main_loop_actor.keyPressed(keyCode,event);
}

void MainScene::keyReleased(EventKeyboard::KeyCode keyCode, Event *event)
{
  main_loop_actor.keyReleased(keyCode,event);
}

void MainScene::update(float dt)
{
  main_loop_actor.act(dt);
}
