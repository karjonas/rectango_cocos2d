#include "SplashScreen.h"

#include "AppMacros.h"
#include "MainScene.h"
#include "LayoutData.h"

#include "2d/CCTransition.h"

USING_NS_CC;

Scene* SplashScreen::scene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    SplashScreen *layer = SplashScreen::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SplashScreen::init()
{
  //////////////////////////////
  // 1. super init first
  if ( !LayerColor::initWithColor(cocos2d::Color4B(77,83,60,255)) )
  {
    return false;
  }

  //Initializing and binding
  auto listener = EventListenerKeyboard::create();

  listener->onKeyReleased = [this](EventKeyboard::KeyCode kc, Event* event){keyReleased(kc,event);};
  _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

  auto origin = Director::getInstance()->getWinSize();

  int32_t scaling = LayoutData::calculate_scaling(origin);
  cocos2d::Label* label = cocos2d::Label::createWithTTF("PRESS ANY KEY", "fonts/PressStart2P.ttf", 8*scaling);

  label->setPosition(origin.width/2, origin.height/2);

  this->addChild(label);

  return true;
}

void SplashScreen::keyReleased(EventKeyboard::KeyCode keyCode, Event *event)
{
  bool exit = false;
  bool start_game = false;

  switch(keyCode)
  {
  case EventKeyboard::KeyCode::KEY_ESCAPE:
    exit = true;
  default:
    start_game = true;
    break;
  }

  if(start_game)
  {
    // fade out the current scene and fade in the new scene in 1 second
    auto newScene = MainScene::scene();
    //auto transition = TransitionScene::create(1.0f, newScene);
    Director::getInstance()->replaceScene(newScene);
  }
  else if (exit)
  {
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
  }
}
