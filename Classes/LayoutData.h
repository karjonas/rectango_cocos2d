#ifndef LAYOUTDATA_H
#define LAYOUTDATA_H

#include "cocos2d.h"

namespace LayoutData
{
  int32_t calculate_scaling(cocos2d::Size screen_size);
  const int32_t max_x = 160;
  const int32_t max_y = 144;
};

#endif // LAYOUTDATA_H
