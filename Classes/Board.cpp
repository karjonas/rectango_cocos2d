#include "Board.h"

#include <cocos2d.h>
#include "2d/CCDrawingPrimitives.h"
#include <iostream>
#include <base/CCDirector.h>
#include <base/CCEventListenerTouch.h>
#include <renderer/CCTextureCache.h>

using namespace cocos2d;

Board::Board() {}

Board::~Board() {}

Board* Board::create()
{
  Board* pSprite = new Board();

  if (pSprite->initWithTexture(nullptr, Rect::ZERO))
  {
    pSprite->autorelease();

    pSprite->initOptions();

    pSprite->addEvents();

    return pSprite;
  }

  CC_SAFE_DELETE(pSprite);
  return NULL;
}

void Board::initOptions()
{
  // do things here like setTag(), setPosition(), any custom logic.
  setContentSize(cocos2d::Size(cocos2d::Vec2(block_width*num_width,block_height*num_height)));
  setAnchorPoint(cocos2d::Vec2(0.5,0.5));
}

void Board::addEvents()
{
  auto listener = cocos2d::EventListenerTouchOneByOne::create();
  listener->setSwallowTouches(true);

  listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
  {
    return false; // FIXME
    cocos2d::Vec2 p = touch->getLocation();
    cocos2d::Rect rect = this->getBoundingBox();

    if(rect.containsPoint(p))
    {
      return true; // to indicate that we have consumed it.
    }

    return false; // we did not consume this event, pass thru.
  };

  listener->onTouchEnded = [=](cocos2d::Touch* touch, cocos2d::Event* event)
  {
    // FIXME ADD:
    //Board::touchEvent(touch);
  };

  cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}

void Board::touchEvent(cocos2d::Touch* touch, cocos2d::Vec2 _point)
{
  CCLOG("touched Board");
}


void Board::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
  _customCommand.init(_globalZOrder);
  _customCommand.func = CC_CALLBACK_0(Board::onDrawPrimitives, this, transform);
  renderer->addCommand(&_customCommand);
}


void Board::init(bool is_upper)
{
  if (is_upper) {
    offset_y += total_height / 2;
    wallColor = cocos2d::Color4F(139 / 255.0f, 149 / 255.0f, 109 / 255.0f, 1.0f);
    floorColor = cocos2d::Color4F(31 / 255.0f, 31 / 255.0f, 31 / 255.0f, 1.0f);

  } else {
    offset_y -= total_height / 2;
    wallColor =  Color4F(31 / 255.0f, 31 / 255.0f, 31 / 255.0f, 1.0f);
    floorColor =  Color4F(139 / 255.0f, 149 / 255.0f, 109 / 255.0f, 1.0f);
  }

  passColor =  Color4F(0.0f,1.0f,0.0f,1.0f);
  stopColor =  Color4F(1.0f,0.0f,0.0f,1.0f);
}

int Board::getHashKey(int h, int w)
{
  return (num_width*h) + w;
}

void Board::setupBoard(std::vector<std::vector<BlockType>> _positions, int unit_x, int unit_y)
{
  positions = _positions;
  unit->pos_x = unit_x;
  unit->pos_y = unit_y;
  unit->board = this;
  unit->place();

  for(auto& kv : customBlocks)
  {
    kv.second->retain();
    kv.second->getParent()->removeChild(kv.second);
    kv.second->release();
  }

  customBlocks.clear();

  auto add_block = [this](CustomBlock* block, int x, int y, int hashKey, BlockType type)
  {
    customBlocks[hashKey] = block;
    block->setAnchorPoint(cocos2d::Vec2(0.5,0.5));
    block->setContentSize(cocos2d::Size(8,8));
    block->setPosition(x,y);
    this->addChild(block, 1, "block");
    block->setOpacity(0);
    block->type = type;
    block->wallColor = cocos2d::Color4B(wallColor);
    block->floorColor = cocos2d::Color4B(floorColor);
  };

  // Add custom actors
  for (int w = 0; w < num_width; w++)
  {
    for (int h = 0; h < num_height; h++)
    {
      int hashKey = getHashKey(h,w);
      int x = w * block_width + block_width/2;
      int y = h * block_height + block_height/2;

      if (positions[w][h] == BlockType::STOP)
      {
        CustomBlock* block = CustomBlock::create();
        add_block(block,x,y, hashKey, BlockType::STOP);
      }
      else if(positions[w][h] == BlockType::PASS)
      {
        CustomBlock* block = CustomBlock::create();
        add_block(block,x,y, hashKey, BlockType::PASS);
      }
      else if(positions[w][h] == BlockType::DEATH)
      {
        CustomBlock* block = CustomBlock::create();
        add_block(block,x,y, hashKey, BlockType::DEATH);
      }
    }
  }
}

bool Board::isCompleted()
{
  return (positions[unit->pos_x][unit->pos_y] == BlockType::EXIT
      && unit->getNumberOfRunningActions() == 0);
}

bool Board::isDead()
{
  return (positions[unit->pos_x][unit->pos_y] == BlockType::DEATH);
}

void Board::flipBlocks() {
  for (int w = 0; w < num_width; w++)
  {
    for (int h = 0; h < num_height; h++)
    {
      int hashKey = getHashKey(h, w);
      if (positions[w][h] == BlockType::STOP)
      {
        CustomBlock* block = customBlocks.at(hashKey);
        block->type = BlockType::PASS;
        positions[w][h] = BlockType::PASS;
      } else if(positions[w][h] == BlockType::PASS)
      {
        CustomBlock* block = customBlocks.at(hashKey);
        block->type = BlockType::STOP;
        positions[w][h] = BlockType::STOP;
      }
    }
  }
}

void Board::onFlipBlock()
{
  flip_block = true;
}

bool Board::isFlipBlock(int w, int h)
{
  return (positions[w][h] == BlockType::PASS);
}

bool Board::isWalkable(int w, int h)
{
  return (positions[w][h] != BlockType::WALL
      && positions[w][h] != BlockType::STOP);
}

bool Board::isCustomBlock(int w, int h)
{
  return (positions[w][h] == BlockType::PASS
          || positions[w][h] == BlockType::STOP
          || positions[w][h] == BlockType::DEATH);
}
void Board::moveLeft()
{
  unit->moveLeft();
}

void Board::moveRight()
{
  unit->moveRight();
}

void Board::moveDown()
{
  unit->moveDown();
}

void Board::moveUp()
{
  unit->moveUp();
}

void Board::draw_block_at(int w, int h, float alpha, Color4F color)
{
  int x = block_width * w;
  int y = h*block_height;
  int x1 = x + block_width;
  int y1 = y + block_height;

  DrawPrimitives::drawSolidRect(Point(x,y), Point(x1,y1),color);
}


void Board::onDrawPrimitives(const kmMat4 &transform)
{
  kmGLPushMatrix();
  kmGLLoadMatrix(&transform);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  drawBoard(1.0f);
}

void Board::drawBoard(float alpha)
{
  if(positions.empty())
    return;

  GLubyte opacity = getOpacity();
  wallColor.a = static_cast<float>(opacity)/255.0f;
  floorColor.a = static_cast<float>(opacity)/255.0f;

  for (int w = 0; w < num_width; w++)
  {
    for (int h = 0; h < num_height; h++)
    {
      if (positions[w][h] == BlockType::WALL)
      {
        draw_block_at(w, h, alpha, wallColor);
      }
      else if (isCustomBlock(w,h) || positions[w][h] == BlockType::EMPTY)
      {
        draw_block_at(w,h, alpha, floorColor);
      }
    }
  }
}

void Board::fadeOutBlocks(float time)
{
  for(auto& kv : customBlocks)
    kv.second->runAction(cocos2d::FadeOut::create(time));
}

void Board::fadeInBlocks(float time)
{
  for(auto& kv : customBlocks)
    kv.second->runAction(cocos2d::FadeIn::create(time));
}
