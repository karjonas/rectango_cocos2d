#include "CustomBlock.h"
#include <cocos2d.h>

using namespace cocos2d;

void CustomBlock::setBounds(int x, int y, int w, int h)
{
  setPosition(x + w/2,y + h/2);
  setContentSize(cocos2d::Size(cocos2d::Vec2(w,h)));
  setAnchorPoint(cocos2d::Vec2(0.5,0.5));
}

void CustomBlock::initOptions()
{

}

void CustomBlock::addEvents()
{

}

void CustomBlock::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
  _customCommand.init(_globalZOrder);
  _customCommand.func = CC_CALLBACK_0(CustomBlock::onDrawPrimitives, this, transform);
  renderer->addCommand(&_customCommand);
}

void CustomBlock::onDrawPrimitives(const cocos2d::Mat4 &transform)
{
  Director::getInstance()->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
  Director::getInstance()->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, transform);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  floorColor.a = getOpacity();
  wallColor.a = getOpacity();

  switch(type)
  {
  case BlockType::STOP:
  {
    cocos2d::DrawPrimitives::drawSolidRect(Point(1,1), Point(7,7), Color4F(wallColor));
    cocos2d::DrawPrimitives::drawSolidRect(Point(2,2), Point(6,6), Color4F(floorColor));
    cocos2d::DrawPrimitives::drawSolidRect(Point(3,3), Point(5,5), Color4F(wallColor));
    break;
  }
  case BlockType::PASS:
  {
    cocos2d::DrawPrimitives::drawSolidRect(Point(3,3), Point(5,5), Color4F(wallColor));
    break;
  }
  case BlockType::DEATH:
  {
    cocos2d::DrawPrimitives::setDrawColor4B(wallColor.r,wallColor.g,wallColor.b,wallColor.a);
    cocos2d::DrawPrimitives::drawLine(Point(1,1), Point(7,7));
    cocos2d::DrawPrimitives::drawLine(Point(1,7), Point(7,1));
    break;
  }
  default:
    break;
  }

  Director::getInstance()->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
}


CustomBlock* CustomBlock::create()
{
  CustomBlock* pSprite = new CustomBlock();

  if (pSprite->initWithTexture(nullptr, cocos2d::Rect::ZERO))
  {

    pSprite->autorelease();

    pSprite->initOptions();

    pSprite->addEvents();


    return pSprite;
  }

  CC_SAFE_DELETE(pSprite);
  return NULL;
}
