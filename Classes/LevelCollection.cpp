#include "LevelCollection.h"
#include "2d/CCTMXTiledMap.h"

#include <2d/CCSprite.h>

#include <2d/CCTMXLayer.h>

#include <iostream>

using namespace cocos2d;

LevelCollection::LevelCollection()
{
  pixelToBlockMap[0]= BlockType::EMPTY;
  pixelToBlockMap[1]= BlockType::UNIT;
  pixelToBlockMap[2]= BlockType::PASS;
  pixelToBlockMap[3]= BlockType::STOP;
  pixelToBlockMap[4]= BlockType::EXIT;
  pixelToBlockMap[5]= BlockType::DEATH;
  pixelToBlockMap[6]= BlockType::WALL;
}

void LevelCollection::readPixmapBoard(bool is_upper, TMXTiledMap* tiled_map)
{
  TMXLayer* layer = tiled_map->getLayer("layer0");

  uint32_t h_offset = is_upper ? 0 : num_height;
  std::vector<std::vector<BlockType>> positions{num_width, {num_height,BlockType::EMPTY}};
  uint32_t unit_x = 0;
  uint32_t unit_y = 0;

  for (size_t h = 0; h < num_height; h++)
  {
    for (size_t w = 0; w < num_width; w++)
    {
      uint32_t GID = layer->getTileGIDAt(Vec2(w, h + h_offset));

      BlockType blockType = pixelToBlockMap.at(GID);

      if (blockType == BlockType::UNIT)
      {
        unit_x = w;
        unit_y = num_height - h - 1;
        blockType = BlockType::EMPTY;
      }

      positions[w][num_height - h - 1] = blockType;
    }
  }

  if (is_upper)
  {
    upper_unit_x = unit_x;
    upper_unit_y = unit_y;
    positionsUpper = positions;
  }
  else
  {
    lower_unit_x = unit_x;
    lower_unit_y = unit_y;
    positionsLower = positions;
  }
}

void LevelCollection::setLevel(int levelId)
{
  auto level_path = "level" + std::to_string(levelId) + ".tmx";
  auto* tiled_map = TMXTiledMap::create(level_path);

  readPixmapBoard(true, tiled_map);
  readPixmapBoard(false, tiled_map);

  tiled_map->retain();
  tiled_map->release();
}
