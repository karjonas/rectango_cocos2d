#include "MainLoopActor.h"

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

#include "LevelCollection.h"
#include "FinishScreen.h"

MainLoopActor::MainLoopActor()
{
  CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect(flipSound.c_str());
  CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect(winSound.c_str());
}

void MainLoopActor::create(cocos2d::Layer& layer)
{
  auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
  auto origin = cocos2d::Director::getInstance()->getVisibleOrigin();

  auto unit_upper = Unit::create();
  auto unit_lower = Unit::create();
  unit_upper->initOptions();
  unit_lower->initOptions();

  upperBoard = Board::create();
  lowerBoard = Board::create();
  upperBoard->initOptions();
  lowerBoard->initOptions();

  upperBoard->stage = &layer; // FIXME: add in constructor as ref
  lowerBoard->stage = &layer;

  lowerBoard->init(false);
  upperBoard->init(true);
  upperBoard->unit = unit_upper;
  lowerBoard->unit = unit_lower;

  upperBoard->addChild(unit_upper, 2,"unit_upper");
  lowerBoard->addChild(unit_lower, 2,"unit_lower");

  layer.addChild(upperBoard, 1, "upperBoard");
  layer.addChild(lowerBoard, 1, "lowerBoard");

  unit_upper->setParent(upperBoard);
  unit_lower->setParent(lowerBoard);

  cocos2d::Vec2 bbox = {upperBoard->getContentSize()/2};
  upperBoard->setPosition(cocos2d::Vec2(visibleSize / 2) + origin + cocos2d::Vec2{0,bbox.y});
  lowerBoard->setPosition(cocos2d::Vec2(visibleSize / 2) + origin - cocos2d::Vec2{0,bbox.y});
}

void MainLoopActor::act(float delta)
{
  if (state == State::FADEINLEVEL)
  {
    if (upperBoard->unit->getNumberOfRunningActions() != 0
        || lowerBoard->unit->getNumberOfRunningActions() != 0
        || upperBoard->unit->getNumberOfRunningActions() != 0
        || lowerBoard->unit->getNumberOfRunningActions() != 0)
      return;


    levels.setLevel(levelId);
    upperBoard->setupBoard(levels.positionsUpper, levels.upper_unit_x, levels.upper_unit_y);
    lowerBoard->setupBoard(levels.positionsLower, levels.lower_unit_x, levels.lower_unit_y);

    upperBoard->runAction(cocos2d::FadeIn::create(fadeInTime));
    lowerBoard->runAction(cocos2d::FadeIn::create(fadeInTime));
    upperBoard->unit->runAction(cocos2d::FadeIn::create(fadeInTime));
    lowerBoard->unit->runAction(cocos2d::FadeIn::create(fadeInTime));
    upperBoard->fadeInBlocks(fadeInTime);
    lowerBoard->fadeInBlocks(fadeInTime);

    state = State::RUNNING;
  }
  else if (state == State::FADEOUTLEVEL)
  {
    if (upperBoard->getNumberOfRunningActions() == 0
        && lowerBoard->getNumberOfRunningActions() == 0)
    {
      if(levelId == maxLevelId)
      {
        auto newScene = FinishScreen::scene();
        cocos2d::Director::getInstance()->replaceScene(newScene);
        state = State::FINSIHED;
      }
      else
      {
        state = State::FADEINLEVEL;
      }
    }
  }
  else if (state == State::RUNNING)
  {
    { // Check if any death
      if(upperBoard->isDead() || lowerBoard->isDead())
      {
        state = State::FADEINLEVEL;
        upperBoard->unit->runAction(cocos2d::FadeOut::create(0.2f));
        lowerBoard->unit->runAction(cocos2d::FadeOut::create(0.2f));
      }
    }

    { // Flip blocks
      bool flipBlocksUpper = lowerBoard->flip_block;
      bool flipBlocksLower = upperBoard->flip_block;

      lowerBoard->flip_block = false;
      upperBoard->flip_block = false;

      if (flipBlocksUpper && !flipBlocksLower)
      {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(flipSound.c_str());
        upperBoard->flipBlocks();
      }
      else if (!flipBlocksUpper && flipBlocksLower)
      {
        lowerBoard->flipBlocks();
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(flipSound.c_str());
      }
    }

    { // Check if level completed
      if (upperBoard->isCompleted() && lowerBoard->isCompleted())
      {
        levelId++;

        upperBoard->runAction(cocos2d::FadeOut::create(0.5f));
        lowerBoard->runAction(cocos2d::FadeOut::create(0.5f));
        upperBoard->unit->runAction(cocos2d::FadeOut::create(0.5f));
        lowerBoard->unit->runAction(cocos2d::FadeOut::create(0.5f));
        upperBoard->fadeOutBlocks(0.5f);
        lowerBoard->fadeOutBlocks(0.5f);

        state = State::FADEOUTLEVEL;

        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(winSound.c_str());
      }
    }

    if (lastPressedKey != cocos2d::EventKeyboard::KeyCode::KEY_YEN)
    {
      keyPressed(lastPressedKey, nullptr);
    }
  }
}

void MainLoopActor::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
  lastPressedKey = keyCode;

  if (state == State::FADEINLEVEL
      || upperBoard->unit->getNumberOfRunningActions() != 0
      || lowerBoard->unit->getNumberOfRunningActions() != 0
      || upperBoard->unit->getNumberOfRunningActions() != 0
      || lowerBoard->unit->getNumberOfRunningActions() != 0)
    return;

  switch (keyCode)
  {
  case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
    upperBoard->moveLeft();
    lowerBoard->moveLeft();
    break;
  case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
    upperBoard->moveRight();
    lowerBoard->moveRight();
    break;
  case cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW:
    upperBoard->moveUp();
    lowerBoard->moveUp();
    break;
  case cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW:
    upperBoard->moveDown();
    lowerBoard->moveDown();
    break;
  default:
    break;
  }
}

void MainLoopActor::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
  if (lastPressedKey == keyCode)
    lastPressedKey = cocos2d::EventKeyboard::KeyCode::KEY_YEN;
}
