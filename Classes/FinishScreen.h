#ifndef FINISHSCREEN_H
#define FINISHSCREEN_H

#include "cocos2d.h"

class FinishScreen : public cocos2d::LayerColor
{
public:
  // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
  virtual bool init();

  // there's no 'id' in cpp, so we recommend returning the class instance pointer
  static cocos2d::Scene* scene();

  // a selector callback
  void menuCloseCallback(Ref* sender);

  void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
  void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

  // implement the "static node()" method manually
  CREATE_FUNC(FinishScreen);

};

#endif // FINISHSCREEN_H
