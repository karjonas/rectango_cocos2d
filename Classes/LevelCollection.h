#ifndef LEVELCOLLECTION_H
#define LEVELCOLLECTION_H

#include "BlockType.h"
#include <map>
#include <vector>
#include <2d/CCTMXTiledMap.h>

using cocos2d::TMXTiledMap;

class LevelCollection
{
public:
  LevelCollection();

  size_t num_width = 20;
  size_t num_height = 9;
  std::vector<std::vector<BlockType>> positionsUpper;
  std::vector<std::vector<BlockType>> positionsLower;

  int32_t currentLevel = 0;

  int32_t upper_unit_x = 0;
  int32_t upper_unit_y = 0;

  int32_t lower_unit_x = 0;
  int32_t lower_unit_y = 0;

  void setLevel(int levelId);

  std::map<uint32_t, BlockType> pixelToBlockMap;

private:
  void readPixmapBoard(bool isLowerBoard,TMXTiledMap* tiled_map);
};

#endif // LEVELCOLLECTION_H
