#include "FinishScreen.h"

#include "AppMacros.h"
#include "LayoutData.h"

USING_NS_CC;

Scene* FinishScreen::scene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    FinishScreen *layer = FinishScreen::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool FinishScreen::init()
{
  //////////////////////////////
  // 1. super init first
  if ( !LayerColor::initWithColor(cocos2d::Color4B(77,83,60,255)) )
  {
    return false;
  }

  //Initializing and binding
  auto listener = EventListenerKeyboard::create();

  listener->onKeyPressed = [this](EventKeyboard::KeyCode kc, Event* event){keyPressed(kc,event);};
  listener->onKeyReleased = [this](EventKeyboard::KeyCode kc, Event* event){keyReleased(kc,event);};
  _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

  auto origin = Director::getInstance()->getWinSize();

  int32_t scaling = LayoutData::calculate_scaling(origin);
  cocos2d::Label* label = cocos2d::Label::createWithTTF("THANKS FOR PLAYING", "fonts/PressStart2P.ttf", 8*scaling);

  label->setPosition(origin.width/2, origin.height/2);

  this->addChild(label);


  return true;
}
void FinishScreen::keyPressed(EventKeyboard::KeyCode keyCode, Event *event)
{
}

void FinishScreen::keyReleased(EventKeyboard::KeyCode keyCode, Event *event)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
