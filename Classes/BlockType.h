#ifndef  BLOCKTYPE_H
#define  BLOCKTYPE_H

enum class BlockType { EMPTY, WALL, UNIT, EXIT, STOP, PASS, DEATH };

#endif //BLOCKTYPE_H
