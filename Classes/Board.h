#ifndef BOARD_H
#define BOARD_H

#include "BlockType.h"
#include "CustomBlock.h"
#include "Unit.h"

#include <vector>
#include <map>

class Board : public cocos2d::Sprite
{
public:
  Board();

  ~Board();

  static Board* create();

  void initOptions();
  void addEvents();
  void touchEvent(cocos2d::Touch* touch, cocos2d::Vec2 _p);

  virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;

  bool isCompleted();

  bool isDead();
  void flipBlocks();
  void onFlipBlock();
  bool isFlipBlock(int w, int h);
  bool isWalkable(int w, int h);
  bool isCustomBlock(int w, int h);
  void moveLeft();
  void moveRight();
  void moveDown();
  void moveUp();
  void draw_block_at(int w, int h, float alpha, cocos2d::Color4F color);
  void init(bool is_upper);
  void setupBoard(std::vector<std::vector<BlockType> > _positions, int unit_x, int unit_y);
  Unit* unit = nullptr;

  void drawBoard(float alpha);
  void fadeOutBlocks(float time);
  void fadeInBlocks(float time);

  int num_width = 20;
  int num_height = 9;

  int block_width = 8;
  int block_height = 8;

  int total_width = num_width * block_width;
  int total_height = num_height * block_height;

  int offset_x = -(num_width * block_width) / 2;
  int offset_y = -(num_height * block_height) / 2;

  cocos2d::Layer* stage = nullptr;

  bool flip_block = false;

private:

  std::vector<std::vector<BlockType>> positions;

  cocos2d::Color4F wallColor;
  cocos2d::Color4F floorColor;
  cocos2d::Color4F passColor;
  cocos2d::Color4F stopColor;

  std::map<int32_t , CustomBlock*> customBlocks;

  cocos2d::CustomCommand _customCommand;
  int getHashKey(int h, int w);
  void onDrawPrimitives(const cocos2d::Mat4 &transform);
};

#endif // BOARD_H
