#ifndef MAINLOOPACTOR_H
#define MAINLOOPACTOR_H

#include "Board.h"
#include "LevelCollection.h"
#include "cocos2d.h"

class MainLoopActor
{
  enum class State
  {
    RUNNING, FADEINLEVEL, FADEOUTLEVEL, FINSIHED
  };

public:
  MainLoopActor();

  Board* upperBoard;
  Board* lowerBoard;

  LevelCollection levels;

  const std::string flipSound = "audio/blip.wav";
  const std::string winSound = "audio/level_win.wav";

  cocos2d::EventKeyboard::KeyCode lastPressedKey = cocos2d::EventKeyboard::KeyCode::KEY_YEN;

  int levelId = 0;
  int maxLevelId = 11;

  State state = State::FADEINLEVEL;

  float fadeInTime = 0.5f;

  void create(cocos2d::Layer& layer);

  void act(float delta);

  void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
  void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

};
#endif // MAINLOOPACTOR_H
