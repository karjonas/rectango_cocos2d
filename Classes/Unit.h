#ifndef UNIT_H
#define UNIT_H

#include <2d/CCSprite.h>

//#include "Board.h"

//using namespace cocos2d;

class Board;

class Unit : public cocos2d::Sprite
{
public:
  Unit() = default;
  ~Unit() = default;
  static Unit* create();

  void initOptions();

  virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;

  void moveLeft();
  void moveRight();
  void moveDown();
  void moveUp();
  void place();

  int32_t pos_x = 0;
  int32_t pos_y = 0;

  int32_t pos_x_prev = 0;
  int32_t pos_y_prev = 0;
  Board* board = nullptr;
  cocos2d::CustomCommand _customCommand;

private:

  void onDrawPrimitives(const cocos2d::Mat4 &transform);
  void tryMoveTo(int32_t x, int32_t y);

};

#endif // UNIT_H
