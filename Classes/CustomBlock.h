#ifndef CUSTOMBLOCK_H
#define CUSTOMBLOCK_H

#include "BlockType.h"
#include <vector>
#include <map>

#include <2d/CCSprite.h>

class CustomBlock : public cocos2d::Sprite
{
public:
  CustomBlock() = default;
  ~CustomBlock() = default;

  void initOptions();
  void addEvents();
  void touchEvent(cocos2d::Touch* touch, cocos2d::Vec2 _p);

  virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;

  void onDrawPrimitives(const cocos2d::Mat4 &transform);

  static CustomBlock* create();

  cocos2d::Color4B wallColor;
  cocos2d::Color4B floorColor;

  // Add type of block
  BlockType type = BlockType::PASS;

private:
  void setBounds(int x, int y, int w, int h);
  cocos2d::CustomCommand _customCommand;

};

#endif // CUSTOMBLOCK_H
