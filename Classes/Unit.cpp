#include "Unit.h"
#include "Board.h"

#include "2d/CCDrawingPrimitives.h"
#include <renderer/CCRenderer.h>

#include <iostream>
#include <base/CCDirector.h>
#include <base/CCEventListenerTouch.h>
#include <2d/CCActionInterval.h>
#include <cocos2d.h>

using namespace cocos2d;

Unit* Unit::create()
{
  Unit* pSprite = new Unit();

  if (pSprite->initWithTexture(nullptr, Rect::ZERO))
  {
    pSprite->autorelease();
    pSprite->initOptions();
    return pSprite;
  }

  CC_SAFE_DELETE(pSprite);
  return NULL;
}

void Unit::initOptions()
{
  setContentSize(cocos2d::Size(Vec2(8,8)));
  setAnchorPoint(Vec2(0.5,0.5));

  // do things here like setTag(), setPosition(), any custom logic.
}

void Unit::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
  //cocos2d::DrawPrimitives::drawLine( Point(0, 0), Point(100, 100) );
  _customCommand.init(_globalZOrder);
  _customCommand.func = CC_CALLBACK_0(Unit::onDrawPrimitives, this, transform);
  renderer->addCommand(&_customCommand);
}

void Unit::onDrawPrimitives(const cocos2d::Mat4 &transform)
{
  Director::getInstance()->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
  Director::getInstance()->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, transform);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  Color4B color(77, 83, 60, getOpacity());
  cocos2d::DrawPrimitives::drawSolidRect(Point(1,1), Point(7,7), Color4F(color));
  Director::getInstance()->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
}

void Unit::tryMoveTo(int32_t x, int32_t y)
{
  if (board == nullptr)
    return;

  auto num_actions = this->getNumberOfRunningActions();
  if(num_actions > 0 || x < 0 || x >= board->num_width || y < 0 || y >= board->num_height)
    return;

  if(board->isWalkable(x,y))
  {
    int32_t new_x = board->block_width*x + 4;
    int32_t new_y = board->block_height*y + 4;

    auto actionTo = MoveTo::create(0.2f,Vec2(new_x,new_y));
    runAction(actionTo);

    if((pos_x - x) != 0)
    {
      auto rotateBy = RotateBy::create(0.2f, -90.0f*(pos_x-x));
      runAction(rotateBy);
    }
    else
    {
      auto rotateBy = RotateBy::create(0.2f, -90.0f*(pos_y-y));
      runAction(rotateBy);
    }

    if(board->isFlipBlock(pos_x, pos_y) && (pos_x_prev != x || pos_y_prev != y))
    {
      board->onFlipBlock();
    }

    pos_x_prev = pos_x;
    pos_y_prev = pos_y;
    pos_x = x;
    pos_y = y;
  }
}

void Unit::moveLeft()
{
  tryMoveTo(pos_x - 1, pos_y);
}

void Unit::moveRight()
{
  tryMoveTo(pos_x + 1, pos_y);
}

void Unit::moveDown()
{
  tryMoveTo(pos_x, pos_y - 1);
}

void Unit::moveUp()
{
  tryMoveTo(pos_x, pos_y + 1);
}

void Unit::place()
{
  int32_t new_x = board->block_width*pos_x + 4;
  int32_t new_y = board->block_height*pos_y + 4;
  setPosition(new_x,new_y);
}
